# Written by Alex at 22nd Dec 2021

# Prepare Adobe Temp Folder on C:\
New-Item -ItemType Directory -Force -Path C:\Temp\Adobe
Copy-Item SOURCEFOLDER -Destination C:\Temp\Adobe -Recurse

# Remove Previous Versoin of Adobe Acrobat Reader
# 32bit Uninstall
C:\Temp\Adobe\AdobeAcroCleaner_DC2015.exe /Silent /Product=1
# 64bit Uninstall
C:\Temp\Adobe\AdobeAcroCleaner_DC2021.exe /Silent /Product=1

# Install Adobe Acrobat Reader
C:\Temp\Adobe\AcroRdrDC2100720099_en_US.exe /sAll /rs /msi EULA_ACCEPT=YES

